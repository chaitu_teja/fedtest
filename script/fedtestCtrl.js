/**
 * Created by CHIDORI on 26-Jun-17.
 */
angular.module('FEDTest')
    .controller('fedtestCtrl', function ($scope,$http) {
        $http.get('list.json').then(function (cats) {
            $scope.cats = cats.data.cats;
        });

    });